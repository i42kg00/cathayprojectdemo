//
//  UIButton+.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/7/23.
//


import UIKit

extension UIView {
    @IBInspectable var masksToBuonds: Bool{
        set{
            self.layer.masksToBounds = newValue
        }
        get {
            return self.layer.masksToBounds
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            self.layer.cornerRadius = newValue
        }
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable var borderWith: CGFloat {
        set {
            self.layer.borderWidth = newValue
        }
        get {
            self.layer.borderWidth
        }
    }
}

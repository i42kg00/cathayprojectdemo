//
//  UserListViewController.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/6/25.
//

import UIKit

class UserListViewController: UIViewController {

    @IBOutlet weak var firstLoginView: UserAddFriendView!
    @IBOutlet weak private var accountView: UserAccountView!
    @IBOutlet weak private var UserFriendTableView: UserFriendsListView!
    private var viewModel: UserListViewModel = UserListViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getUserData { result in
            switch result {
            case .success:
                self.firstLoginView.delegate = self
                self.UserFriendTableView.setData()
                self.accountView.setData(model: self.viewModel.userMan)
                
            case .failure(let error):
                print("error==\(error)")
            }
        }
       
    }
}
extension UserListViewController: UserAddFriendViewDelegate{
    
}


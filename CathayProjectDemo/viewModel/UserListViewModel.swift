//
//  UserListViewModel.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/7/17.
//

import Foundation



class UserListViewModel{
    var userMan:UserManViewModel = UserManViewModel()
    
     init(){
       
    }
    
    func getUserData(completion: BaseViewModel.Completion? = nil){
        userDataResponse { result in
            switch result {
            case .success(let value):
                    self.userMan = value
                completion?(.success)
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    
    private func userDataResponse(completion: @escaping(Result<UserManViewModel, Error>)-> Void){
        let manJson = APIRequest.getUserDataModel(.user)
        guard let url = URL(string: manJson) else{return }
        URLSession.shared.dataTask(with: url) { data, response,error in
            if let error {
                completion(.failure(error))
                return
            }
            guard let response = response as? HTTPURLResponse,(200...299).contains(response.statusCode) else {return}
            guard let data else {return }
            do {
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(UserManViewModel.self, from: data)
                completion(.success(jsonData))
            }catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
    
}

//enum MockError: Error{
//    case noResponse
//    case badnetwork
//}

class BaseViewModel {
    typealias Completion = (_ result : Result)->Void
    
    enum Result{
        case success
        case failure(Error)
    }
}



struct UserManViewModel:Codable {
    var name: String?
    var kokoid: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case kokoid = "kokoid"
    }
}

//
//  APIRequest.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/7/17.
//

import Foundation
enum UserJson: String {
    case user = "man"
    case firstFriend = "friend1"
    case secondFriend = "friend2"
    case dataAndfriendList = "friend3"
    case noDataAndFriendList = "friend4"
    
}

class APIRequest {

    static func getUserDataModel(_ type: UserJson)->String{
        let url = "https://dimanyen.github.io/\(type.rawValue).json"
        return url
    }

    
    
}

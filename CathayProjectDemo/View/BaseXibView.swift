//
//  BaseXibView.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/6/28.
//



import UIKit

@IBDesignable
class BaseXibView: UIView {
    
    @IBInspectable var nibName: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetUp()
    }
    
    required init?(coder aCoder: NSCoder) {
        super.init(coder: aCoder)
        xibSetUp()
    }
    
    
    
    
    func xibSetUp() {
        let bundle = Bundle(for: self.classForCoder)
        if nibName == nil {
            nibName = "\(self.classForCoder)"
        }
        let nib = UINib(nibName: nibName!, bundle: bundle)
        if let view = nib.instantiate(withOwner: self).first as? UIView {
            view.frame = self.bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(view)
        }
    }
}

//
//  UserAccountView.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/7/11.
//

import UIKit

class UserAccountView: BaseXibView {

    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var userName: UILabel!
    @IBOutlet weak private var stackView: UIStackView!
    @IBOutlet weak private var userIDLabel: UILabel!
    
    func setData(model: UserManViewModel){
        userName.text = model.name
        userIDLabel.text = model.kokoid
        let view = UserInviteFriendView()
        stackView.addArrangedSubview(view)
    }
}

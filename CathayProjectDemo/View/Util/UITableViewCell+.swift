//
//  UITableViewCell+.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/7/11.
//

import UIKit

extension UITableViewCell {
    static var name: String {
        return String(describing: self)
    }
}

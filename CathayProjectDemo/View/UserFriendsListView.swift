//
//  UserInviteFriendsView.swift
//  CathayProjectDemo
//
//  Created by 葉育彣 on 2023/6/29.
//

import UIKit
import Foundation

class UserFriendsListView: BaseXibView{

    @IBOutlet weak private var addFriendBtn: UIButton!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(UserFriendsListViewCell.self)
    }
   
    
    func setData(){

    }
    
    
    

}

extension UserFriendsListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UserFriendsListViewCell = tableView.dequeueResableCell(for: indexPath)
        
        return cell
    }
    
    
}
